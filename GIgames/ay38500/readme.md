The AY-3-8500-1 Pong-on-a-chip IC

Created by General Instruments in ~1975

Original die images provided by Sean Riddle @(http://seanriddledecap.blogspot.com/2017/02/blog-post.html), licensed under CC BY-SA 4.0

Annotation and processing work was done by Cole Johnson, again liscensed under CC BY-SA 4.0

(https://creativecommons.org/licenses/by-sa/4.0/)

This chip has been processed into a javascript simulation!
Find it here: (http://nerdydelights.droppages.com/ChipSim/AY38500/AY-3-8500.html)
Source code: (https://gitlab.com/TheProgrammerIncarnate/VisualP0NG)