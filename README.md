# Chip Processing

Various files used to create IC simulations/emulations

License/source image information is in each directory's readme.md file

All files besides scripts are derivatives of source images

All python processing scripts are CC0 1.0 (aka public domain)