from PIL import Image, ImageDraw
import math
import sys
#wire, empty,dif+wire,dif,wire+via,pullup+wire,pullup+wire+via,all 4 pull,metal+dif+trans,metal+trans
w = (106, 243, 41) #wire
empty = (83, 97, 124) #empty
dw = (255, 255, 255) #dif+wire
d = (134, 83, 82) #dif
wvd = (2, 36, 210) #wire+via+dif
pw = (244, 242, 38) #pullup+wire
pwd = (244, 149, 38) #pullup+wire+dif PWNED!!
pwdv = (244, 38, 48) #pullup+wire+dif+via
mdt = (173, 39, 243) #metal+dif+trans
mt = (39, 188, 243) #metal+trans
test = (0, 0, 0)

#Via -5, max 14 * 14
#Wide vias *
#Pullup transistors
#Logic transistors *
#Wire smoothing 1/2
#Diagonals
def replaceLongVia(xPos,yPos):
	#Overwrite via
	xOff = 0
	yOff = 0
	xPos = xPos - 2
	while(xOff<22):
		while(yOff<8):
			pixel = px[xPos+xOff,yPos+yOff]
			if(pixel==wvd):
				px[xPos+xOff,yPos+yOff] = w
			yOff = yOff + 1
		yOff = 0
		xOff = xOff + 1
	#Then draw a rectangle
	xOff = 0
	yOff = 1
	while(xOff<22):
		while(yOff<7):
			px[xPos+xOff,yPos+yOff] = wvd
			yOff = yOff + 1
		yOff = 1
		xOff = xOff + 1
def replaceCircleVia(xPos,yPos):
	#Overwrite circle
	xOff = 0
	yOff = 0
	xPos = xPos - 5
	while(xOff<14):
		while(yOff<14):
			pixel = px[xPos+xOff,yPos+yOff]
			if(pixel==wvd):
				px[xPos+xOff,yPos+yOff] = dw
			elif(pixel==pwdv):
				px[xPos+xOff,yPos+yOff] = pwd
			yOff = yOff + 1
		yOff = 0
		xOff = xOff + 1
	#Then draw a square
	xOff = 2
	yOff = 2
	while(xOff<12):
		while(yOff<12):
			pixel = px[xPos+xOff,yPos+yOff]
			if(pixel==dw):
				px[xPos+xOff,yPos+yOff] = wvd
			elif(pixel==pwd):
				px[xPos+xOff,yPos+yOff] = pwdv
			yOff = yOff + 1
		yOff = 2
		xOff = xOff + 1

def isCorner(xPos,yPos,empty1,c1,c2):#w dw
	if(px[xPos,yPos]!=empty and px[xPos,yPos]!=empty1):
		return False
	elif(xPos < 4 or yPos < 4 or xPos > (width - 4) or yPos > (height - 4)):
		return False
	elif(px[xPos-1,yPos]==c1 or px[xPos-1,yPos]==c2):
		if(px[xPos,yPos+1]==c1 or px[xPos,yPos+1]==c2):
			if((px[xPos+1,yPos+1]!=c1 and px[xPos+1,yPos+1]!=c2) and (px[xPos-1,yPos-1]!=c1 and px[xPos-1,yPos-1]!=c2)):
				return True
		elif(px[xPos,yPos-1]==c1 or px[xPos,yPos-1]==c2):
			if((px[xPos+1,yPos-1]!=c1 and px[xPos+1,yPos-1]!=c2) and (px[xPos-1,yPos+1]!=c1 and px[xPos-1,yPos+1]!=c2)):
				return True
	elif(px[xPos+1,yPos]==c1 or px[xPos+1,yPos]==c2):
		if(px[xPos,yPos+1]==c1 or px[xPos,yPos+1]==c2):
			if((px[xPos+1,yPos-1]!=c1 and px[xPos+1,yPos-1]!=c2) and (px[xPos-1,yPos+1]!=c1 and px[xPos-1,yPos+1]!=c2)):
				return True
		elif(px[xPos,yPos-1]==c1 or px[xPos,yPos-1]==c2):
			if((px[xPos+1,yPos+1]!=c1 and px[xPos+1,yPos+1]!=c2) and (px[xPos-1,yPos-1]!=c1 and px[xPos-1,yPos-1]!=c2)):
				return True
	return False

def isElbow(xPos,yPos,f1,f2,empty1):#w dw
	c1 = empty1
	c2 = empty
	if(px[xPos,yPos]!=f1 and px[xPos,yPos]!=f2):
		return False
	elif(xPos < 4 or yPos < 4 or xPos > (width - 4) or yPos > (height - 4)):
		return False
	elif(px[xPos-1,yPos]==c1 or px[xPos-1,yPos]==c2):
		if(px[xPos,yPos+1]==c1 or px[xPos,yPos+1]==c2):
			if((px[xPos+1,yPos+1]!=c1 and px[xPos+1,yPos+1]!=c2) and (px[xPos-1,yPos-1]!=c1 and px[xPos-1,yPos-1]!=c2)):
				return True
		elif(px[xPos,yPos-1]==c1 or px[xPos,yPos-1]==c2):
			if((px[xPos+1,yPos-1]!=c1 and px[xPos+1,yPos-1]!=c2) and (px[xPos-1,yPos+1]!=c1 and px[xPos-1,yPos+1]!=c2)):
				return True
	elif(px[xPos+1,yPos]==c1 or px[xPos+1,yPos]==c2):
		if(px[xPos,yPos+1]==c1 or px[xPos,yPos+1]==c2):
			if((px[xPos+1,yPos-1]!=c1 and px[xPos+1,yPos-1]!=c2) and (px[xPos-1,yPos+1]!=c1 and px[xPos-1,yPos+1]!=c2)):
				return True
		elif(px[xPos,yPos-1]==c1 or px[xPos,yPos-1]==c2):
			if((px[xPos+1,yPos+1]!=c1 and px[xPos+1,yPos+1]!=c2) and (px[xPos-1,yPos-1]!=c1 and px[xPos-1,yPos-1]!=c2)):
				return True
	return False
def doThickenTrans(xPos,yPos,isPullup):#w dw
	if(isPullup):
		c1 = pwd
		c2 = pwdv
	else:
		c1 = mdt
		c2 = mt
	if(px[xPos,yPos]!=dw):
		return False
	elif(xPos < 4 or yPos < 4 or xPos > (width - 4) or yPos > (height - 4)):
		return False
	elif((px[xPos+1,yPos+1]==c1 and (px[xPos+2,yPos]==c1 or px[xPos+2,yPos]==c2) and (px[xPos,yPos+2]==c1 or px[xPos,yPos+2]==c2)) or (px[xPos-1,yPos-1]==c1 and (px[xPos-2,yPos]==c1 or px[xPos-2,yPos]==c2) and (px[xPos,yPos-2]==c1 or px[xPos,yPos-2]==c2)) or (px[xPos+1,yPos-1]==c1 and (px[xPos+2,yPos]==c1 or px[xPos+2,yPos]==c2) and (px[xPos,yPos-2]==c1 or px[xPos,yPos-2]==c2)) or (px[xPos-1,yPos+1]==c1 and (px[xPos-2,yPos]==c1 or px[xPos-2,yPos]==c2) and (px[xPos,yPos+2]==c1 or px[xPos,yPos+2]==c2))):
		if(isPullup==False):
			px[xPos,yPos] = mdt
		elif(px[xPos,yPos]==wvd):
			px[xPos,yPos] = pwdv
		else:
			px[xPos,yPos] = pwd
	return False

#Code start
#print(sys.getTime())
path = "fixed.png"#sp0256.png"
image = Image.open(path)
px = image.load()
draw = ImageDraw.Draw(image)
width,height = image.size;
x = 0
y = 0
while(y<height):
	while(x<width):
		pixel = px[x,y]
		if((pixel==wvd or pixel==pwdv) and (px[x-1,y]!=wvd and px[x-1,y]!=pwdv) and (px[x-1,y+1]==wvd or px[x-1,y+1]==pwdv)):
			if(px[x-2,y+1]==wvd or px[x-2,y+1]==pwdv):
				replaceCircleVia(x,y)
			else:
				replaceLongVia(x,y)
		else:
			#Smooth metal corners
			if(isCorner(x,y,d,w,dw)):
				if(pixel==d):
					px[x,y] = dw
				else:
					px[x,y] = w
			#Smooth metal elbows
			elif(isElbow(x,y,w,dw,d)):
				if(pixel==dw):
					px[x,y] = d
				else:
					px[x,y] = empty
			doThickenTrans(x,y,False)
		x = x + 1
	x = 0
	y = y + 1
	if(y%1000==0):
		print("Round 1, Line: "+str(y))
x = 0
y = 0
print("Loop 1 Done")
wasInGate = False
while(y<height):
	while(x<width):
		pixel = px[x,y]
		#Smooth dif corners
		if(isCorner(x,y,w,d,dw)):
			if(pixel==w):
				px[x,y] = dw
			else:
				px[x,y] = d
		#Smooth dif elbows
		elif(isElbow(x,y,d,dw,w)):
			if(pixel==dw):
				px[x,y] = w
			else:
				px[x,y] = empty
		if(wasInGate==False and pixel==mt):
			wasInGate = True
			if(px[x-1,y]!=mdt):
				px[x-1,y] = mt
		elif(wasInGate and pixel!=mt):
			wasInGate = False
			if(pixel!=mdt):
				px[x,y] = mt
		x = x + 1
	x = 0
	y = y + 1
	if(y%1000==0):
		print("Round 2, Line: "+str(y))
print("Loop 2 Done")
#Gate widening, round 2
x = 0
y = 0
wasInGate = False
while(x<width):
	while(y<height):
		pixel = px[x,y]
		if(wasInGate==False and pixel==mt):
			wasInGate = True
			if(px[x,y-1]!=mdt and (px[x-1,y-1]==mdt or px[x-1,y-1]==mt)):
				px[x,y-1] = mt
		elif(wasInGate and pixel!=mt):
			wasInGate = False
			if(pixel!=mdt and (px[x-1,y]==mdt or px[x-1,y]==mt)):
				px[x,y] = mt
		elif(pixel==dw):
			px[x,y] = (200,200,200)
		y = y + 1
	y = 0
	x = x + 1
px[177,448] = (255,0,0)
px[283,562] = (0,0,255)
image.save("output.png", "png")
#print(sys.getTime())
